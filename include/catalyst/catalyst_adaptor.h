 
 /* File:   CatalsytAdaptor.h
 * Author: Jose J. Camata (camata@nacad.ufrj.br)
 *
 * Created on June 2, 2016, 11:46 AM
 */

#ifndef CATALYSTADAPTOR_H
#define	CATALYSTADAPTOR_H



#include "libmesh/libmesh.h"
#include "libmesh/mesh.h"
#include "libmesh/equation_systems.h"

#include <iostream>
#include <string>
#include <vector>

#include "config.h"

#ifdef HAVE_CATALYST

using namespace libMesh;
using namespace std;

namespace CatalystAdaptor
{

  void mark_to_rebuild_grid();

  void Initialize(int numScripts, std::vector<string> visualizationScripts);

  void Finalize();

  void CoProcess(EquationSystems &eq, double time, unsigned int timeStep, bool lastTimeStep);
  
}

#endif 
#endif	/* CATALYSTADAPTOR_H */

