
#ifndef HDF5_HELPER_H__
#define HDF5_HELPER_H__

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <string.h>

/* convenience macro to handle errors */
#define ERROR(FNAME)                                              \
do {                                                              \
    int _errno = errno;                                           \
    fprintf(stderr, #FNAME " failed at line %d, errno=%d (%s)\n", \
        __LINE__, _errno, _errno?strerror(_errno):"ok");          \
    return 1;                                                     \
} while(0)


#include "libmesh/libmesh_config.h"

#ifdef LIBMESH_HAVE_HDF5

#include <hdf5.h>

#ifdef H5Z_ZFP_USE_PLUGIN

#include "H5Zzfp_plugin.h"

#endif


hid_t setup_filter(int n, hsize_t *chunk, 
                    int zfpmode,
                    double rate, double acc, uint prec,
                    uint minbits, 
                    uint maxbits, 
                    uint maxprec, 
                    int minexp);


int H5_WriteInteger(hid_t fid, hid_t cpid, const char *datasetname, int *dataset, long size, bool using_compression);

int H5_WriteDouble(hid_t fid, hid_t cpid, const char *datasetname, double *dataset, long size, bool using_compression);

#endif

#endif 
