#
# libMesh Find Module
#
# Usage:
#    Control the search through LIBMESH_DIR to the libMesh 
#    installation prefix.
#
#    This module does not search default paths!
#
#    Following variables are set:
#    LIBMESH_FOUND          (BOOL)       Flag indicating if libMesh was found
#    LIBMESH_INCLUDE        (LIST)       List of all required include files
#    LIBMESH_LDFLAGS        (LIST)       List of all required libMesh libraries
#    LIBMESH_CXX                         C++ Compiler used to build libMesh
#    LIBMESH_CXXFLAGS       (LIST)       C++ Flags used to build libMesh
# #############################################################################

# Standard CMake modules see CMAKE_ROOT/Modules
include(FindPackageHandleStandardArgs)

set(LIBMESH_FOUND OFF)

if(NOT LIBMESH_DIR)
    set(LIBMESH_DIR $ENV{LIBMESH_DIR})
endif()

if (EXISTS "${LIBMESH_DIR}" )

    find_program( LIBMESH_CONFIG_EXECUTABLE
        NAMES libmesh-config
        HINTS ${LIBMESH_DIR}
        PATH_SUFFIXES bin
        DOC "libmesh-config executable" )

        if (LIBMESH_CONFIG_EXECUTABLE )

            exec_program( ${LIBMESH_CONFIG_EXECUTABLE}
                ARGS --cxx
                OUTPUT_VARIABLE LIBMESH_CXX
                RETURN_VALUE LMC_INC_RET
            )
            # SET(CMAKE_CXX_COMPILER ${LIBMESH_CXX})

            exec_program( ${LIBMESH_CONFIG_EXECUTABLE}
                ARGS --cxxflags
                OUTPUT_VARIABLE LIBMESH_CPP
                RETURN_VALUE LMC_INC_RET
            )
            SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LIBMESH_CPP}")
  
            exec_program( ${LIBMESH_CONFIG_EXECUTABLE}
                ARGS --include
                OUTPUT_VARIABLE LIBMESH_INC
                RETURN_VALUE LMC_INC_RET
            )
            SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LIBMESH_INC}")
            exec_program( ${LIBMESH_CONFIG_EXECUTABLE}
                ARGS --libs
                OUTPUT_VARIABLE LIBMESH_LIBS
                RETURN_VALUE LMC_INC_RET
            )
            #SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LIBMESH_LIBS}")
            
            #SET(CMAKE_EXE_LINKER_FLAGS ${LIBMESH_LIBS})
        endif()

        set(LIBMESH_FOUND ON)

else()
    message(STATUS "libMesh_DIR=${libMesh_DIR} does not exist")                
endif()

find_package_handle_standard_args(libMesh
  REQUIRED_VARS LIBMESH_FOUND LIBMESH_CXX
)

