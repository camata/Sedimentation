# Input file for transport simulation

version = 2.0

#######################################################
# Mesh Parameters
#------------------------------------------------------

# The coarse mesh from which to start adaptivity
mesh/file = canal_ufrgs.msh

# Mesh Refinement Parameters
amr/initial_unif_ref_mesh     = 3
amr/max_r_steps = 2
amr/r_fraction  = 0.9
amr/c_fraction  = 0.01
amr/max_h_level = 3
amr/r_interval  = 10
amr/amrc_flow_transp = true
amr/first_step_refinement = false
amr/refine_only_elements_on_deposition = true
amr/pressure_weight = 0.0 # must be in the range [0.0, 1.0]. Zero means negleting pressure to compute element's flux error
amr/ratio_flagged_elements_to_amrc = 0.5 # %

#------------------------------------------------------------
# 2D
# Boundary Condition
# canal_ufrgs.msh
# Physical parameters:
# 1 1 "INLET"
# 1 2 "NOSLIP"
# 1 3 "DEPOSITION"
# 1 4 "SLIPY"
# 1 5 "OUTFLOW"
# 2 6 "FLUID"

#-----------------------------------
# Flow Boundary Condition
flow/dirichlet/inlet          = '1'
#flow/dirichlet/inlet/constant/u = 1.0
flow/dirichlet/inlet/constant/v = 0.0
flow/dirichlet/inlet/function/u = 5.50640625*(y-6.25*y*y)
flow/dirichlet/inlet/useRamp = true
flow/dirichlet/inlet/tRamp   =  9.0 # dimensionless time amount for the ramp function. Zero means no ramp at all. This value must be smaller than total simulation time (tmax)
# Warning: the denominator MUST be equal the "tRamp" value defined above
flow/dirichlet/inlet/function/uRamp = (2*t/9-t*t/(9*9))
flow/dirichlet/slipy          = '4'
flow/dirichlet/noslip         = '2 3'
flow/dirichlet/pressure/zero  = '4'
flow/neumann/apply_bc = true
flow/neumann/outlet          = '5'
flow/dirichlet/pressure/pin = false
flow/dirichlet/pressure/pin_pressure_coord_x = 0.0
flow/dirichlet/pressure/pin_pressure_coord_y = 1.0
flow/dirichlet/pressure/pin_pressure_coord_z = 0
flow/dirichlet/pressure/pin_pressure_value = 0.0
flow/has_initial_condition   = true
#sediment transport boundary condition
transport/deposition = '3'
transport/dirichlet/prescribed = '1'
transport/dirichlet/prescribed/value = 1.0
transport/neumann/apply_bottom_flux = false

transport/has_initial_condition = true

#-------------------------------
#Mesh Moviment
mesh/enable_moviment           = false
mesh/dirichlet/nodisplacement  = '4'

#--------------------------------
#Time Step Control
ts_control/model_name = PC11
ts_control/dt_min = 0.0001
ts_control/dt_max = 0.01
ts_control/tol_u = 0.005
ts_control/tol_s = 0.001
ts_control/nsa_max = 10
ts_control/kp = 0.075
ts_control/ki = 0.175
ts_control/kd = 0.01
ts_control/pc11_theta = 0.95
ts_control/alpha = 0.95
ts_control/k_exp = 2.0
ts_control/s_mim = 0.05
ts_control/s_max = 1.05
ts_control/complete_flow_norm = false

#----------------------------------------------------
# TIME INTEGRATION PARAMETERS
time/n_flow_sstate     = 50
time/n_transport_sstate     = 50
time/deltat       = 0.0001
time/init_time    = 0.0
time/tmax         = 273
time/n_time_steps = 100000000
time/theta        = 1.0

#----------------------------------------------------
# Linear and Non-Linear parameters   

flow_n_nonlinear_steps        = 11
flow_nonlinear_tolerance      = 1.0e-4
transport_n_nonlinear_steps   = 11
transport_nonlinear_tolerance = 1.0E-4
max_linear_iterations         = 200
flow_initial_linear_solver_tolerance       = 1.0E-2
transport_initial_linear_solver_tolerance       = 1.0E-2
minimum_linear_solver_tolerance       = 1.0E-6
linear_tolerance_power = 1.1
divergenge_tolerance = 2.0E+2
t_step_start_check_divergenge = 50

#------------------------------------------------------
# Stabilization Parameters
stabilization/dt_stab = 1.0
stabilization/fem_model = SUPG/PSPG
stabilization/yzBeta = 1.0
stabilization/s_ref_bar_yzBeta = 1.0
stabilization/delta_transient_factor = .5
stabilization/LSIC = 1.0

#------------------------------------------------------
# EQUATION PARAMETERS
Froude      =  0.0
Reynolds    =  0.0
Grashof     =  1.258e+9
Schmidt     =  1.0
Us          =  0.0 #1.72e-3
ex          =  0.0
ey          = -1.0
c_factor    =  1.0
Cs          =  0.2
xlock       =  0.0
hlock       =  0.16

# Write out every nth timestep to file.
write_interval    = 20
catalyst_interval = 10
stats_interval = 5

