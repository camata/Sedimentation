# libMesh-Sedimentation #
## An polydisperse sediment transport solver ##


libmesh-sedimentation (version 1.0) is based on libMesh library.
 libMesh is an opensource code (http://libmesh.github.io/) that provides a framework for the 
numerical simulation of partial differential equations using arbitrary unstructured discretizations 
on serial and parallel platforms. The library makes use of high-quality, existing software whenever possible. 
PETSc or the Trilinos Project are used for the solution of linear systems on both serial and parallel platforms. 
The libMesh library was first created at The University of Texas at Austin in the CFDLab in March 2002. 
libmesh-sedimentation has been developed at COPPE/UFRJ and considers both message passing and OpenMP. 

For contact {alvaro, camata}@nacad.ufrj.br, are the responsible persons for libMesh-sedimentation


cmake ../Sedimentation -DLIBMESH_DIR=/opt/lib/libmesh/1.6.1 -DParaView_DIR=/opt/lib/catalyst-5.9.0/lib/cmake/paraview-5.9/  -DZFP_ROOT_DIR=/opt/lib/zfp/  -DH5Z_ZFP_PLUGIN_DIR=/opt/lib/hdf5-plugin -DOSMESA_INCLUDE_DIR=/opt/lib/osmesa/include -DOSMESA_LIBRARY=/opt/lib/osmesa/lib/x86_64-linux-gnu/libOSMesa.so



Cmake ParaView Catalyst 5.8:

cmake ../ParaView-v5.8.1 -DPARAVIEW_BUILD_SHARED_LIBS=1 -DPARAVIEW_USE_PYTHON=1 -DPARAVIEW_USE_MPI=1 -DCMAKE_BUILD_TYPE=Release -DPARAVIEW_BUILD_EDITION=CATALYST_RENDERING -DPARAVIEW_INSTALL_DEVELOPMENT_FILES=1 -DPARAVIEW_USE_QT=0 -DVTK_OPENGL_HAS_OSMESA=1 -DVTK_USE_X=0 -DVTK_USE_OFFSCREEN=1 -DCMAKE_INSTALL_PREFIX=/sw/apps/suse/shared/catalyst/5.8 -DOPENGL_INCLUDE_DIR= -DOPENGL_gl_LIBRARY= -DOPENGL_glu_LIBRARY= -DOSMESA_INCLUDE_DIR=/sw/apps/suse/shared/mesa/17.0.0/gnu/include -DOSMESA_LIBRARY=/sw/apps/suse/shared/mesa/17.0.0/gnu/lib/libOSMesa.so -DVTK_DEFAULT_RENDER_WINDOW_OFFSCREEN=1




